from django.contrib.auth.forms import UserCreationForm, UserChangeForm, AuthenticationForm, PasswordResetForm, SetPasswordForm
from blog.models import CustomUser, Post
from django.forms import ModelForm
from django import forms


class CustomUserCreationForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(CustomUserCreationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={'class': 'form-control', })
        self.fields['first_name'].widget = forms.TextInput(attrs={'class': 'form-control', 'required': 'true'})
        self.fields['first_name'].help_text = 'Required'
        self.fields['last_name'].widget = forms.TextInput(attrs={'class': 'form-control', 'required': 'true'})
        self.fields['last_name'].help_text = 'Required'
        self.fields['email'].widget = forms.EmailInput(attrs={'class': 'form-control', 'required': 'true'})
        self.fields['email'].help_text = 'Required'
        self.fields['password1'].widget = forms.PasswordInput(attrs={'class': 'form-control', })
        self.fields['password2'].widget = forms.PasswordInput(attrs={'class': 'form-control', })

    class Meta:
        model = CustomUser
        fields = UserCreationForm.Meta.fields + ('email', 'first_name', 'last_name')


class CustomPasswordResetForm(PasswordResetForm):

    def __init__(self, *args, **kwargs):
        super(CustomPasswordResetForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget = forms.EmailInput(attrs={'class': 'form-control', 'required': 'true'})

    class Meta:
        model = CustomUser
        fields = ('email',)


class CustomSetPasswordForm(SetPasswordForm):

    def __init__(self, *args, **kwargs):
        super(CustomSetPasswordForm, self).__init__(*args, **kwargs)
        self.fields['new_password1'].widget = forms.PasswordInput(attrs={'class': 'form-control', 'required': 'true'})
        self.fields['new_password2'].widget = forms.PasswordInput(attrs={'class': 'form-control', 'required': 'true'})

    class Meta:
        model = CustomUser
        fields = ('new_password1', 'new_password2')


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('username', 'email')


class PostForm(ModelForm):

    class Meta:
        model = Post
        exclude = ('author', 'slug', 'published_date', 'category')
        widgets = {
            'title': forms.TextInput(attrs={
                'class': 'form-control',
                'required': 'true',
            }),
            'preview': forms.Textarea(attrs={
                'class': 'form-control',
            }),
            'content': forms.Textarea(attrs={
                'class': 'form-control',
                'required': 'true',
            }),
            'category_list': forms.TextInput(attrs={
                'class': 'form-control',
            }),
            'draft': forms.CheckboxInput(attrs={
                'class': 'form-control',
            }),
            'image': forms.ClearableFileInput(attrs={
                'class': 'form-control-file',
            })
        }
        labels = {
            'category_list': 'Сategory list',
            'title': 'Title',
            'preview': 'Preview',
            'content': 'Text',
            'draft': 'Draft'
        }
        help_texts = {
            'category_list': 'Categories must be separated by a comma. For example: python, django, blog',
            'preview': 'This is the text that will appear on the main page before the "Read more" button',
            'draft': 'Click here if you want to save the post as a draft',
        }


class LoginForm(AuthenticationForm):
    username = forms.CharField(
        label="Username",
        max_length=30,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true',
    }))
    password = forms.CharField(
        label="Password",
        max_length=30,
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'required': 'true',
    }))


class ContactForm(forms.Form):
    from_email = forms.EmailField(required=True, widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true',
    }))
    subject = forms.CharField(required=True, widget=forms.TextInput(attrs={
            'class': 'form-control',
            'required': 'true'
    }))
    message = forms.CharField(required=True, widget=forms.Textarea(attrs={
            'class': 'form-control',
            'required': 'true'
    }))
