from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.auth import get_user_model
# from django.utils.text import slugify
from pytils.translit import slugify


# Create your models here.
class CustomUserManager(UserManager):
    pass


class CustomUser(AbstractUser):
    objects = CustomUserManager()
    # add additional fields in here

    def __str__(self):
        return self.username


class Category(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(default=slugify(name), unique=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Сategories"


class Post(models.Model):
    author = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE
    )
    category = models.ManyToManyField(Category, blank=True)  # Allow blank submission in admin
    title = models.CharField(max_length=200)
    slug = models.SlugField(default=slugify(title), unique=True)
    preview = models.TextField(blank=True)
    content = models.TextField()
    category_list = models.TextField(blank=True)
    published_date = models.DateTimeField(blank=True, null=True)
    draft = models.BooleanField(default=False)

    def user_directory_path(self, filename):
        # file will be uploaded to MEDIA_ROOT/author/<filename>
        return 'user_{0}/{1}'.format(self.author, filename)

    image = models.ImageField(upload_to=user_directory_path, blank=True)

    def save(self, *args, **kwargs):

        slug = slugify(self.title)
        while True:
            try:
                post = Post.objects.get(slug=slug)
                if post == self:
                    self.slug = slug
                    break
                else:
                    slug = slug + '-'
            except:
                self.slug = slug
                break

        super(Post, self).save(*args, **kwargs)

    def __str__(self):
        return self.title