from django.urls import path, include, re_path
from blog import views
from django.contrib.auth.views import LoginView, LogoutView
from blog.forms import LoginForm

urlpatterns = [
    path('my_posts/', views.MyPostsView.as_view(), name='my_posts'),
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('accounts/login/', LoginView.as_view(authentication_form=LoginForm), name='login'),
    path('accounts/logout/', LogoutView.as_view(next_page='post_list'), name='logout'),
    path('create/', views.PostCreate.as_view(), name='post_create'),
    path('delete/<slug:slug>/', views.PostDeleteView.as_view(), name='post_delete'),
    path('edit/<slug:slug>', views.PostEdit.as_view(), name='post_edit'),
    path('search/', views.SearchView.as_view(), name='search'),

    path('email/', views.email_view, name='email'),
    path('success/', views.success_view, name='success'),
    # Reset password
    path('accounts/password_reset/', views.CustomResetPasswordView.as_view(), name='password_reset'),
    re_path('accounts/reset/(?P<uidb64>[^/]+)/(?P<token>[^/]+)/$', views.CustomPasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('accounts/', include('django.contrib.auth.urls')),

    path('<slug:slug>/', views.PostDetailView.as_view(), name='post_detail'),
    path('', views.PostListView.as_view(), name='post_list'),
]