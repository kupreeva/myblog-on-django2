# to run tests
# python manage.py test blog.tests.test_forms -v 2

from django.test import TestCase
from django.contrib.auth import get_user_model

from blog.forms import CustomUserCreationForm, LoginForm, ContactForm, PostForm, CustomSetPasswordForm, CustomPasswordResetForm


class RegistrationFormTest(TestCase):

    def test_invalid_registration_form(self):
        """test invalid data in registration form"""
        invalid_data = {
            "username": "testusername",
            "first_name": "testfirstname",
            "last_name": "testlastname",
            "email": "examplemail@gmail.com",
            "password1": "secret12WW",
            "password2": "not secret12WW"
        }
        form = CustomUserCreationForm(data=invalid_data)
        form.is_valid()
        self.assertTrue(form.errors)

    def test_valid_registration_form(self):
        """test valid data in registration form"""
        valid_data = {
            "username": "testusername",
            "first_name": "testfirstname",
            "last_name": "testlastname",
            "email": "examplemail@gmail.com",
            "password1": "secret12WW",
            "password2": "secret12WW"
        }
        form = CustomUserCreationForm(data=valid_data)
        form.is_valid()
        self.assertFalse(form.errors)


class LoginFormTest(TestCase):

    def test_invalid_login_form(self):
        """test invalid data in login form"""
        invalid_data = {
            "username": "testusername",
            "password": "",
        }
        form = LoginForm(data=invalid_data)
        form.is_valid()
        self.assertTrue(form.errors)

    def test_valid_login_form(self):
        """test valid data in login form"""
        get_user_model().objects.create_user(username='testusername', password='secret12WW')
        valid_data = {
            "username": "testusername",
            "password": "secret12WW"
        }
        form = LoginForm(data=valid_data)
        self.assertTrue(form.is_valid())


class ContactFormTest(TestCase):

    def test_invalid_contact_form(self):
        """test invalid data in contact form"""
        invalid_data = {
            "from_email": "",
            "subject": "test subject",
            "message": "test message"
        }
        form = ContactForm(data=invalid_data)
        self.assertFalse(form.is_valid())

    def test_valid_contact_form(self):
        """test valid data in contact form"""
        valid_data = {
            "from_email": "example@gmail.com",
            "subject": "test subject",
            "message": "test message"
        }
        form = ContactForm(data=valid_data)
        self.assertTrue(form.is_valid())


class PostFormTest(TestCase):

    def test_invalid_post_form(self):
        """test invalid data in post form"""
        invalid_data = {
            "title": "",
            "content": "test content",
        }
        form = PostForm(data=invalid_data)
        self.assertFalse(form.is_valid())

    def test_valid_post_form(self):
        """test valid data in post form"""
        valid_data = {
            "title": "test title",
            "content": "test content",
        }
        form = PostForm(data=valid_data)
        self.assertTrue(form.is_valid())

    def test_post_form_fields_labels(self):
        """test that labels are displayed correctly"""
        form = PostForm()
        self.assertTrue(form.fields['category_list'].label == 'Сategory list')
        self.assertTrue(form.fields['title'].label == 'Title')
        self.assertTrue(form.fields['preview'].label == 'Preview')
        self.assertTrue(form.fields['content'].label == 'Text')
        self.assertTrue(form.fields['draft'].label == 'Draft')

    def test_post_form_help_text(self):
        """test that help texts are displayed correctly"""
        form = PostForm()
        self.assertEqual(form.fields['category_list'].help_text,
                         'Categories must be separated by a comma. For example: python, django, blog')
        self.assertEqual(form.fields['preview'].help_text,
                         'This is the text that will appear on the main page before the "Read more" button')
        self.assertEqual(form.fields['draft'].help_text, 'Click here if you want to save the post as a draft')


class CustomSetPasswordFormTest(TestCase):

    def test_invalid_setpassword_form(self):
        """test invalid data in setpassword form"""
        user = get_user_model().objects.create_user(username='testusername', password='secret12WW')
        invalid_data = {
            "new_password1": "Secretpass90",
            "new_password2": "noSecretpass90",
        }
        form = CustomSetPasswordForm(user=user, data=invalid_data)
        self.assertFalse(form.is_valid())

    def test_valid_setpassword_form(self):
        """test valid data in setpassword form"""
        user = get_user_model().objects.create_user(username='testusername', password='secret12WW')
        valid_data = {
            "new_password1": "Secretpass90",
            "new_password2": "Secretpass90",
        }
        form = CustomSetPasswordForm(user=user, data=valid_data)
        self.assertTrue(form.is_valid())


class CustomPasswordResetFormTest(TestCase):

    def test_invalid_passwordreset_form(self):
        """test invalid data in CustomPasswordResetForm"""
        invalid_data = {
            "email": "email",
        }
        form = CustomPasswordResetForm(data=invalid_data)
        self.assertFalse(form.is_valid())

    def test_valid_passwordreset_form(self):
        """test valid data in CustomPasswordResetForm"""
        valid_data = {
            "email": "example@gmail.com",
        }
        form = CustomPasswordResetForm(data=valid_data)
        self.assertTrue(form.is_valid())