# to run tests
# python manage.py test blog.tests.test_models -v 2

from django.test import TestCase

from blog.models import Post, CustomUser, Category
from django.contrib.auth import get_user_model


class CustomUserModelTest(TestCase):

    def test_str_user(self):
        """
        testing __str__ func in CustomUser model
        """
        user = get_user_model().objects.create_user(username='testuser', password='testpassword')
        self.assertTrue(isinstance(user, CustomUser))
        self.assertEqual(str(user), user.username)


class CategoryModelTest(TestCase):

    def create_category(self):
        return Category.objects.create(name='test category')

    def test_str_cat(self):
        """
        testing __str__ func in Category model
        """
        cat = Category.objects.create(name='cat_test')
        self.assertTrue(isinstance(cat, Category))
        self.assertEqual(str(cat), cat.name)

    def test_fields_labels(self):
        """
        testing the labels for all the fields
        """
        cat = self.create_category()

        # Get the metadata for the required field and use it to query the required field data
        name_field_label = cat._meta.get_field('name').verbose_name
        # Compare the value to the expected result
        self.assertEquals(name_field_label, 'name')

        slug_field_label = cat._meta.get_field('slug').verbose_name
        self.assertEquals(slug_field_label, 'slug')

    def test_name_max_length(self):
        """
        testing title max length to ensure that it was implemented as planned
        """
        cat = self.create_category()
        max_length = cat._meta.get_field('name').max_length
        self.assertEquals(max_length, 50)


class PostModelTest(TestCase):

    def setUp(self):
        get_user_model().objects.create_user(username='testuser', password='testpassword')

    def create_post(self):
        return Post.objects.create(author=get_user_model().objects.get(username__exact='testuser'), title='test_title', content='Test content')

    def test_str_post(self):
        """
        testing __str__ func in Post model
        we tested whether the created title matched the expected title
        """
        post = self.create_post()
        # Is the post object is an instance of Post?
        self.assertTrue(isinstance(post, Post))
        self.assertEqual(str(post), post.title)

    def test_user_directory_path(self):
        """
        testing user_directory_path func in Post model
        """
        post = self.create_post()
        file = 'imgfile'
        self.assertEqual(post.user_directory_path(filename=file), 'user_{0}/{1}'.format(post.author, file))

    def test_fields_labels(self):
        """
        testing the labels for all the fields
        """
        post = self.create_post()

        title_field_label = post._meta.get_field('title').verbose_name
        self.assertEquals(title_field_label, 'title')

        author_field_label = post._meta.get_field('author').verbose_name
        self.assertEquals(author_field_label, 'author')

        category_field_label = post._meta.get_field('category').verbose_name
        self.assertEquals(category_field_label, 'category')

        slug_field_label = post._meta.get_field('slug').verbose_name
        self.assertEquals(slug_field_label, 'slug')

        preview_field_label = post._meta.get_field('preview').verbose_name
        self.assertEquals(preview_field_label, 'preview')

        content_field_label = post._meta.get_field('content').verbose_name
        self.assertEquals(content_field_label, 'content')

        category_list_field_label = post._meta.get_field('category_list').verbose_name
        self.assertEquals(category_list_field_label, 'category list')

        published_date_field_label = post._meta.get_field('published_date').verbose_name
        self.assertEquals(published_date_field_label, 'published date')

        draft_field_label = post._meta.get_field('draft').verbose_name
        self.assertEquals(draft_field_label, 'draft')

        image_field_label = post._meta.get_field('image').verbose_name
        self.assertEquals(image_field_label, 'image')

    def test_title_max_length(self):
        """
        testing title max length to ensure that it was implemented as planned
        """
        post = self.create_post()
        max_length = post._meta.get_field('title').max_length
        self.assertEquals(max_length, 200)