# to run tests
# python manage.py test blog.tests.test_views -v 2

from django.test import SimpleTestCase, TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.utils import timezone

from blog.models import Post, CustomUser

# Create your tests here.


class SignUpPage(SimpleTestCase):
    allow_database_queries = True

    def test_signup_page_status_code(self):
        """we test that signup page exists and returns a 200 HTTP status code"""
        response = self.client.get('/signup/')
        self.assertEquals(response.status_code, 200)

    def test_view_url_by_name(self):
        """we confirm that signup page uses the url named signup"""
        response = self.client.get(reverse('signup'))
        self.assertEquals(response.status_code, 200)

    def test_view_uses_correct_template(self):
        """we check that the template used is register.html"""
        response = self.client.get(reverse('signup'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/register.html')


class LoginPage(SimpleTestCase):
    allow_database_queries = True

    def test_login_page_status_code(self):
        """we test that login page exists and returns a 200 HTTP status code"""
        response = self.client.get('/accounts/login/')
        self.assertEquals(response.status_code, 200)

    def test_view_url_by_name(self):
        """we confirm that login page uses the url named login"""
        response = self.client.get(reverse('login'))
        self.assertEquals(response.status_code, 200)

    def test_view_uses_correct_template(self):
        """we check that the template used is login.html"""
        response = self.client.get(reverse('login'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html')


class PasswordResetPage(SimpleTestCase):
    allow_database_queries = True

    def test_passwordreset_page_status_code(self):
        """we test that passwordreset page exists and returns a 200 HTTP status code"""
        response = self.client.get('/accounts/password_reset/')
        self.assertEquals(response.status_code, 200)

    def test_view_url_by_name(self):
        """we confirm that passwordreset page uses the url named password_reset"""
        response = self.client.get(reverse('password_reset'))
        self.assertEquals(response.status_code, 200)

    def test_view_uses_correct_template(self):
        """we check that the template used is password_reset_form.html"""
        response = self.client.get(reverse('password_reset'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/password_reset_form.html')


class SuccessPage(SimpleTestCase):
    allow_database_queries = True

    def test_success_page_status_code(self):
        """we test that success page exists and returns a 200 HTTP status code"""
        response = self.client.get('/success/')
        self.assertEquals(response.status_code, 200)

    def test_view_url_by_name(self):
        """we confirm that success page uses the url named success"""
        response = self.client.get(reverse('success'))
        self.assertEquals(response.status_code, 200)

    def test_view_uses_correct_template(self):
        """we check that the template used is success.html"""
        response = self.client.get(reverse('success'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'blog/success.html')


class PostListViewTest(TestCase):

    def setUp(self):
        get_user_model().objects.create_user(username='testuser', password='testpassword')

        # Create 7 posts for pagination tests
        number_of_posts = 7
        for post_id in range(number_of_posts):
            Post.objects.create(
                author=get_user_model().objects.get(username__exact='testuser'),
                title='test_title №{}'.format(post_id),
                content='Test content №{}'.format(post_id)
            )

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('post_list'))
        self.assertEqual(response.status_code, 200)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('post_list'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'blog/post_list.html')

    def test_pagination_is_five(self):
        response = self.client.get(reverse('post_list'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'] is True)
        self.assertTrue(len(response.context['my_post_list']) == 5)

    def test_lists_all_posts(self):
        # Get second page and confirm it has (exactly) remaining 2 items
        response = self.client.get(reverse('post_list') + '?page=2')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'] is True)
        self.assertTrue(len(response.context['my_post_list']) == 2)


class MyPostsViewTest(TestCase):

    def setUp(self):
        get_user_model().objects.create_user(username='testuser', password='testpassword')

        # create 5 Post objects
        number_of_posts = 5
        for post_id in range(number_of_posts):
            Post.objects.create(
                author=get_user_model().objects.get(username__exact='testuser'),
                title='test_title №{}'.format(post_id),
                content='Test content №{}'.format(post_id),
                published_date=timezone.now()
            )

    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('my_posts'))
        self.assertRedirects(response, '/accounts/login/?next=/my_posts/')

    def test_logged_in_uses_correct_template(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('my_posts'))

        # Check our user is logged in
        self.assertEqual(str(response.context['user']), 'testuser')
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

        # Check we used correct template
        self.assertTemplateUsed(response, 'blog/my_posts.html')

    def test_only_user_posts_in_list(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('my_posts'))

        # Check our user is logged in
        self.assertEqual(str(response.context['user']), 'testuser')
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

        # Check we have posts and have no drafts
        self.assertTrue('user_post_list' in response.context)
        self.assertTrue('user_draft_list' in response.context)
        self.assertEqual(len(response.context['user_draft_list']), 0)

        # Confirm all posts belong to testuser
        for post in response.context['user_post_list']:
            self.assertEqual(response.context['user'], post.author)

    def test_posts_ordered_by_published_date(self):

        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('my_posts'))

        # Check our user is logged in
        self.assertEqual(str(response.context['user']), 'testuser')
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

        # Check that posts ordered by published_date
        # order_by('-published_date')
        last_date = 0
        for post in response.context['user_post_list']:
            if last_date == 0:
                last_date = post.published_date
            else:
                self.assertTrue(last_date >= post.published_date)
                last_date = post.published_date

        # Check that drafts ordered by pk
        # order_by('-pk')
        last_pk = 0
        for post in response.context['user_draft_list']:
            if last_pk == 0:
                last_pk = post.pk
            else:
                self.assertTrue(last_pk >= post.published_date)
                last_pk = post.published_date