# Generated by Django 2.1 on 2018-08-26 13:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20180826_1458'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': 'Category', 'verbose_name_plural': 'Сategories'},
        ),
        migrations.AlterModelOptions(
            name='post',
            options={},
        ),
    ]
