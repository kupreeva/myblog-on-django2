from django.utils import timezone

from django.urls import reverse_lazy, reverse
from django.views import generic
from blog.forms import CustomUserCreationForm, PostForm, CustomPasswordResetForm, CustomSetPasswordForm, ContactForm
from blog.models import Post, Category
from django.contrib.auth import login, authenticate
from django.contrib.auth.views import PasswordResetView, PasswordResetConfirmView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
import operator
from functools import reduce
from django.core.mail import send_mail, BadHeaderError
from django.shortcuts import render, redirect
from django.http import HttpResponse


# Create your views here.
class SearchView(generic.ListView):
    model = Post
    context_object_name = 'my_post_list'
    paginate_by = 5
    template_name = 'blog/post_list.html'

    def get_queryset(self):
        result = Post.objects.filter(draft=False).order_by('-published_date')
        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(title__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(content__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(preview__icontains=q) for q in query_list))
            )
        return result

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['search_query'] = self.request.GET.get('q')

        return context_data


class MyPostsView(LoginRequiredMixin, generic.ListView):
    model = Post
    template_name = 'blog/my_posts.html'

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['user_draft_list'] = self.model.objects.filter(author=self.request.user, draft=True).order_by('-pk')
        context_data['user_post_list'] = self.model.objects.filter(author=self.request.user, draft=False).order_by('-published_date')

        return context_data


class PostDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Post
    success_url = reverse_lazy('my_posts')

    def get_queryset(self):
        return self.model.objects.filter(author=self.request.user)


class PostEdit(LoginRequiredMixin, generic.UpdateView):
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('post_list')

    def get_context_data(self, **kwargs):
        context_data = super(PostEdit, self).get_context_data(**kwargs)
        context_data['action'] = reverse('post_edit',
                                    kwargs={'slug': self.get_object().slug})

        return context_data

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        post = form.save(commit=False)

        #post.author = self.request.user
        if not form.cleaned_data.get('draft', False):
            post.published_date = timezone.now()
        # need to save your Post model first, then add the category instances to the many to many relation.
        post.save()

        cat_list = form.cleaned_data['category_list']
        cat_list = cat_list.split(',')
        for cat in cat_list:
            if Category.objects.filter(name=cat).exists():
                print("cat exists")
                c = Category.objects.filter(name=cat).get()
                post.category.add(c)
            else:
                print("no such cat")
                c = Category.objects.create(name=cat)
                post.category.add(c)

        post.save()
        # form_valid method should return an HttpResponse.
        return super(PostEdit, self).form_valid(form)


class CustomResetPasswordView(PasswordResetView):
    form_class = CustomPasswordResetForm


class CustomPasswordResetConfirmView(PasswordResetConfirmView):
    form_class = CustomSetPasswordForm


class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('post_list')
    template_name = 'registration/register.html'

    # to login user directly after registration we have to override form_valid()
    def form_valid(self, form):
        valid = super(SignUp, self).form_valid(form)
        username, password = form.cleaned_data.get('username'), form.cleaned_data.get('password1')
        new_user = authenticate(username=username, password=password)
        login(self.request, new_user)
        return valid


class PostListView(generic.ListView):
    model = Post
    context_object_name = 'my_post_list'
    paginate_by = 5

    def get_queryset(self):
        result = Post.objects.filter(draft=False).order_by('-published_date')
        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(title__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(content__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(preview__icontains=q) for q in query_list))
            )
        return result

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        context_data['search_query'] = self.request.GET.get('q')

        return context_data


class PostDetailView(generic.DetailView):
    model = Post


class PostCreate(LoginRequiredMixin, generic.CreateView):
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('post_list')

    def get_context_data(self, **kwargs):
        context_data = super(PostCreate, self).get_context_data(**kwargs)
        context_data['action'] = reverse('post_create')

        return context_data

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        post = form.save(commit=False)

        post.author = self.request.user
        if not form.cleaned_data.get('draft', False):
            post.published_date = timezone.now()
        # need to save your Post model first, then add the category instances to the many to many relation.
        post.save()

        cat_list = form.cleaned_data['category_list']
        if cat_list:
            cat_list = cat_list.split(',')
            for cat in cat_list:
                if Category.objects.filter(name=cat).exists():
                    print("cat exists")
                    c = Category.objects.filter(name=cat).get()
                    post.category.add(c)
                else:
                    print("no such cat")
                    c = Category.objects.create(name=cat)
                    post.category.add(c)

        post.save()
        # form_valid method should return an HttpResponse.
        return super(PostCreate, self).form_valid(form)


def email_view(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
                send_mail(subject, message, from_email, ['admin@example.com'])
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return redirect('success')
    return render(request, "blog/email.html", {'form': form})


def success_view(request):
    return render(request, "blog/success.html")

