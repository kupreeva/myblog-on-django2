# Blog
##This is a django blog 

Technologies 
* Python 3.7
* Django 2.1
* Bootstrap
* MySQL

Features
- [x] Create blog post
- [x] Edit blog post
- [x] Post drafts
- [x] Delete post
- [x] Publish blog post
- [x] View list of posts
- [x] Detailed view of post
- [x] Search for post
- [x] Log in
- [x] Sign up
- [x] Login user directly after registration
- [ ] Comments
- [ ] Social authentication
- [x] Reset password
- [ ] Unit tests
- [x] Slug fields
- [x] Pagination
- [x] Add picture to the post
- [x] Contact form


